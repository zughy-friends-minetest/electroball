electroball.add_points = function(arenaid,teamid,points)
    local arena = arena_lib.mods["electroball"].arenas[arenaid]
    local color
    if teamid == 1 then
        color = 0xf47e1b
    else
        color = 0x28ccdf
    end
    arena_lib.HUD_send_msg_all("broadcast", arena, string.upper( arena.teams[teamid].name .. " scores!"), 3, nil, color)
    arena.teams[teamid].points = arena.teams[teamid].points + points

    arena.resetjob[1] = core.after(3,function()
        electroball.add_ball(arena)
        arena_lib.HUD_send_msg_all("title", arena, "Go!", 1, nil, "0xE6482E")
        arena.resetjob[1] = nil
    end)

    core.after(2,function()
        arena_lib.HUD_send_msg_all("title", arena, "1", 1, nil, "0xE6482E")
    end)
    core.after(1,function()
        arena_lib.HUD_send_msg_all("title", arena, "2", 1, nil, "0xE6482E")
    end)
    arena_lib.HUD_send_msg_all("title", arena, "3", 1, nil, "0xE6482E")
end




function electroball.refresh(arena)
    if arena.resetjob[1] then 
        job = arena.resethob[1]
        job:cancel()
    end
    -- reset points
    for team_id,stats in pairs(arena.teams) do
        arena.teams[team_id].points = 0
    end
    -- clear objects
    for _,obj in pairs(core.get_objects_in_area(arena.clear_1, arena.clear_2)) do
        if not(obj:is_player()) then
            if obj:get_luaentity().name == "electroball:ball" then
                obj:remove()
            end
        end
    end
    -- spawn new entities
    electroball.add_ball(arena)
    electroball.add_counters(arena)
    -- spawn players at locations
    for pl_name,stats in pairs(arena.players) do
        local player = core.get_player_by_name(pl_name)
        arena_lib.teleport_onto_spawner(player, arena)
    end
end

function electroball.add_counters(arena)
    math.randomseed(core.get_us_time())
    local uuid = math.random()
    arena.counter_uuid = uuid
    local obj = core.add_entity(vector.add(arena.red_pts_1, vector.new(0, 2, 0)),
                        "electroball:teampoints", core.write_json({
        _arena_id = arena_lib.get_arena_by_name("electroball", arena.name),
        _arena_name = arena.name,
        _team_id = 1,
        _digits = 1,
        _uuid = uuid,
    }))

    obj:set_yaw(arena.red_rot*3.14159*0.5)

    obj = core.add_entity(vector.add(arena.red_pts_2, vector.new(0, 2, 0)),
    "electroball:teampoints", core.write_json({
        _arena_id = arena_lib.get_arena_by_name("electroball", arena.name),
        _arena_name = arena.name,
        _team_id = 1,
        _digits = 2,
        _uuid = uuid,
    }))

    obj:set_yaw(arena.red_rot*3.14159*0.5)

    obj = core.add_entity(vector.add(arena.blue_pts_1, vector.new(0, 2, 0)),
                        "electroball:teampoints", core.write_json({
        _arena_id = arena_lib.get_arena_by_name("electroball", arena.name),
        _arena_name = arena.name,
        _team_id = 2,
        _digits = 1,
        _uuid = uuid,
    }))

    obj:set_yaw(arena.blue_rot*3.14159*0.5)

    obj = core.add_entity(vector.add(arena.blue_pts_2, vector.new(0, 2, 0)),
        "electroball:teampoints", core.write_json({
        _arena_id = arena_lib.get_arena_by_name("electroball", arena.name),
        _arena_name = arena.name,
        _team_id = 2,
        _digits = 2,
        _uuid = uuid,
    }))

    obj:set_yaw(arena.blue_rot*3.14159*0.5)
end

function electroball.add_ball(arena)
    math.randomseed(core.get_us_time())
    local uuid = math.random()
    arena.ball_uuid = uuid
    
    core.add_entity(arena.ball_start, "electroball:ball",
                        core.write_json({
        _arena_id = arena_lib.get_arena_by_name("electroball", arena.name),
        _arena_name = arena.name,
        _uuid = uuid,
    }))

end