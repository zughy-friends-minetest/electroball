This is a sample minigame for arena_lib. its purpose is to make it easy for others to make minigames using arena_lib
===============
It also includes the arena_lib api documentation in DOC

It looks complicated, but its not. There are a lot of empty folders and empty files (with code samples, that are not run).
this is because its supposed to be a template for anyone to quickly make a minigame.
if this sample minigame were stripped down to only the code required and not all the explanation and template, 
then it would be very small and not scary-looking :P

To start quickly, open init.lua and read the comments. init.lua is *mostly* comments ...
Then when you are ready to start programming, keep DOCS.md open in another tab for reference.

When you are done programming, go ahead and delete all the excessive comments... 

The code here is CC0, by MisterE. Most of the commentary is written by MisterE, but some is copied from the DOCS, which are GPLv3

As regards the code, no attribution is required or expected. As far as MisterE is concerned, any minigame you make with this template is *your* work. 

The reward will be more minigames to play!


After you have examined the code and begun your own minigame, go ahead and delete the files you dont need, and create files you do. You probably dont want to delete the libraries folder. You will want to delete the DOC folder, the README, any callback files in minigame_manager that you are not calling from init.lua, the nodes and items files if you arent using them, and the models and textures folders if you arent using them. You will definately want to change init.lua, and any callback files in minigame_manager that you *are* using, as well as the license.

Everywhere you see the name of the mod "sample_minigame" you will want to replace it with your own modname, which is the same as the minigame name. If you want to register two minigames at once, I suggest you make a modpack with an api mod for common functions, and a mod for each minigame you want, all bundled.



Notes:

An important concept for arena_lib is that arena_lib does not actually define an in-world arena. Instead, it defines minigame spawnpoints. It is up to you to make an inescapable arena and when creating the arena, set the spawnpoints there. You *can* write code to place a schematic in the on_load callback... which could be how your minigame resets the arena (if its even possible to break blocks in the arena: it doesnt have to be!)



Creating an arena:
=================
Arena_lib provides a User interface for creating and editing arenas. As the mod author, you have to register the commands to start the editor, (thats done in init.lua). This is already done for you in this minigame. 


Here is a tutorial for creating an arena with this minigame :
(and it really applies to any minigame, only a few parts are different, and those will be described)

1) Build your arena. 
-------------------

Make it in-escapable. Provided with this mod is a node called "sample_minigame:wall" with the description "Minigame Blocker"
Use it to surround your arena (use worldedit)

for testing purposes, use the command providied with this mod to place the sample arena:

`/place_sample_minigame_schem`

it will place the schem at your position (you may have to turn around to see it)

![step_1](/screenshots/step_1.png)

2) Create an arena
-------------------
`/sample_minigame create GreatArena`

the syntax is `/<modname> create <arenaname>`
for most minigames, depending on if they follow the command format provided in init.lua

3) Edit the arena
-------------------
`/sample_minigame edit GreatArena`

the syntax is `/<modname> edit <arenaname>`

This will open the editor.

![step_3](/screenshots/step_3.png)

The editor has several parts, which are given in tools that you use

* players
        allows to set the number of players
* spawners
        set the spawner locations
* signs
        place and initialize the arena sign. Players will enter the arena by punching the arena sign and waiting for the queue.
* background music
        enable background music... your mod must provide this in the sounds folder. The bgm will loop, and you can set the gain and volume
* settings
        Where you set things like arena properties, rename the arena, and set the arena author
* print arena info
* enable arena
        you can only do this when spawners and players and signs have been set up, you probably dont want to do it until the settings have been checked also (some minigames require you to set positons in the settings)
* leave editor without enabling
        you can return with the edit command

4) Set the sign location first
---------------
use (punch with) the signs icon. You have an add signs tool, a remove signs tool, and a sign node. Place the sign node at an accessible location where players frequent so they can play the minigame. Then punch the sign with the add signs tool to set the sign as belonging to this arena.

![step_4](/screenshots/step_4.png)

use the go_back tool

5) Then set the players number
--------------------

Use the players tool (the first one in your inventory)

You are given a min_players setting, a max_players setting and a number changer. Notice that above the HUD bar there is a message
that tells you the current number. Use the number changer to change the current number: left-click it to decrease the number, right click it to increase the current number. Note that you cannot be pointing at anything when you use the number changer tool or it wont change.

Then with the current number set, use the min_players or max_players tool to set it to that number. Note that if teams are enabled, then these numbers are per team.

![step_5](/screenshots/step_5.png)

6) make the spawner locations
--------------
go back to the main menu by using the go-back tool, and then use the second tool in your inventory, the spawners tool.

Enter your arena that you built or placed. Move around the arena, adding spawners by using the add spawner tool. You can also delete spawners with the other two tools.

![step_6](/screenshots/step_6.png)

7) edit the arena settings
--------------


Use the gear icon from the main menu, and you are in the setting menu. The first tool edits the arena name and the author. The second tool edits the arena properties (that you defined in the minigame registration in init.lua) For any minigame, the most important thing is to check the arnea properties and set the settings correctly. If you do not, then bad things can happen, like if you do not enter the locations correctly for a minigame that sets a schematic or clears an area, then the wrong or loo large an area can be cleared or set.

For the sample minigame, there is no such danger; just open the properties to be able to change the starting_lives value.

*caution* for minigames that have positions, when you open the properties, the postions are listed as 

`{ y = int, x = int, z = int}`

That is confusing, being out of order. Best just to erase everything on the brakets and retype the values in the correct order.

![step_7](/screenshots/step_7.png)

7.5) If the minigame time mode is "decremental" then use the clock icon to set the countdown timer
----------------
8) Enable the arena
--------------------
go back to the main menu and use the green checkmark tool to enable the arena.

9) set the hub_spawn_point and the queue time 
---------------------
(you need to only do this the first time you enable an arena for a minigame)

enter the command
`/minigamesettings sample_minigame`

the syntax is `/minigamesettings <modname>`

![step_9](/screenshots/step_9.png)

set the two settings there:

*`hub_spawn_point` where players are teleported after the game is finished

*`queue_waiting_time` how many seconds to wait for other players to join the match once the minimum number of players have joined the queue


10) Play!
--------------

Now the arena is ready to play!

Get another player and both of you punch the sign. The queue will start and then you will be teleported into the arena. To exit the queue, punch the sign again. To leave the minigame while playing, type `/quit` to send a chat message to global chat type 

`/all <msg>`

minigame chat is by default separate from global chat.

![step_10](/screenshots/step_10.png)





