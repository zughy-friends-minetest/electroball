


local modname = "electroball"
electroball = {}
electroball.punch_timeout = .2

arena_lib.register_minigame( modname , {

        prefix = "[EB]",

        --icon = "mod_icon",

        teams = {"red","blue"},

        teams_color_overlay = {"#a93b3b","#394778"},

        is_team_chat_default = true,

        in_game_physics = {
                speed = .8,
        },

        end_when_too_few = false,

        endless = true,

        hotbar = {
                slots = 0,
                background_image = "blank.png",
                selected_image = "blank.png",
        },

        join_while_in_progress = true,

        spectate_mode = "all",

        disable_inventory = true,

        keep_attachments = true,

        show_nametags = true,

        time_mode = "none",

        load_time = 2,

        -- celebration_time = 5,

        -- in_game_physics: (table) a physical override to set to each player when they enter an arena, following the Minetest physics_override parameters

        disabled_damage_types = {"fall","punch"},

        properties = {

                ball_start = vector.new(0,0,0),

                red_pts_1 = vector.new(0,0,0),
                red_pts_2 = vector.new(0,0,0),

                blue_pts_1 = vector.new(0,0,0),
                blue_pts_2 = vector.new(0,0,0),

                clear_1 = vector.new(0,0,0),
                clear_2 = vector.new(0,0,0),

                blue_rot = 0, -- rot of the score ents in multiples of pi
                red_rot = 2,

                ball_uuid = 0,
                counter_uuid = 0,

        },

        temp_properties = {
                resetjob = {},
        },

        player_properties = {
                old_props = {},
                old_flags = {},
                last_punch = 0,
        },

        team_properties = {
                points = 0,
        },
})










--====================================================
--====================================================
--            Calling the other files
--====================================================
--====================================================


local path = core.get_modpath(modname)


dofile(path .. "/api.lua")
dofile(path .. "/nodes.lua")
dofile(path .. "/items.lua")
dofile(path .. "/minigame_manager.lua")








