local punch_timeout = electroball.punch_timeout

local function set_up_player(arena,p_name)
    local player = core.get_player_by_name(p_name)
    if player then
        arena.players[p_name].old_props = player:get_properties()
        arena.players[p_name].old_flags = player:hud_get_flags()

        player:set_properties({
            collisionbox = {
                -0.3 * 0.3, -0.0001, -0.3 * 0.3, 0.3 * 0.3, 1.7 * 0.3, 0.3 * 0.3
            },
            selectionbox = {
                -0.3 * 0.3, 0.0, -0.3 * 0.3, 0.3 * 0.3, 1.7 * 0.3, 0.3 * 0.3
            },
            visual_size = vector.new(.3, .3, .3),
            eye_height = .4,
            collide_with_objects = false,
            physical = false,
            stepheight = 0,
        })
        player:hud_set_flags({
            hotbar = false,
            healthbar = false,
            wielditem = false,
        })
        player:get_inventory():set_size("hand", 1)
        player:get_inventory():set_stack("hand", 1, "electroball:hand")
    end
end

arena_lib.on_load("electroball", function(arena)
    arena_lib.HUD_send_msg_all("title", arena, "ELECTROBALL", 3, nil, "0xF47E1B")

    for pl_name, stats in pairs(arena.players) do
        set_up_player(arena,pl_name)
    end
end)

arena_lib.on_join("electroball", function(p_name, arena, as_spectator, was_spectator)
    if not as_spectator then
        set_up_player(arena,p_name)
    end
end)

arena_lib.on_start("electroball", function(arena)
    -- clear objects
    for _,obj in pairs(core.get_objects_in_area(arena.clear_1, arena.clear_2)) do
        if not(obj:is_player()) then
                obj:remove()
        end
    end
    electroball.add_ball(arena)
    electroball.add_counters(arena)
end)

-- arena_lib.on_celebration("electroball", function(arena, winners)

--     for pl_name, stats in pairs(arena.players) do
--         local player = core.get_player_by_name(pl_name)
--         if player then
--             player:set_properties(arena.players[pl_name].old_props)
--             player:hud_set_flags(arena.players[pl_name].old_flags)

--         end
--     end
-- end)

arena_lib.on_prequit("electroball", function(arena, p_name, is_spectator, reason)
    local player = core.get_player_by_name(p_name)
    if player then 
        player:set_properties(arena.players[p_name].old_props)
        player:hud_set_flags(arena.players[p_name].old_flags)
    end
    return true
end)

-- speed boost
controls.register_on_press(function(player, control_name)
    if control_name == "aux1" and
        arena_lib.is_player_in_arena(player:get_player_name(), "electroball") and
        not (arena_lib.is_player_spectating(player:get_player_name())) then
        local p_name = player:get_player_name()
        local arena = arena_lib.get_arena_by_player(p_name)
        if arena and arena.in_game and core.get_gametime() + punch_timeout >
            arena.players[p_name].last_punch then
            local look = player:get_look_dir()
            look.y = 0
            player:add_player_velocity(look)
            arena.players[p_name].last_punch = core.get_gametime()
        end
    end
end)

-- hitting other players
core.register_on_punchplayer(function(player, hitter, time_from_last_punch,
                                          tool_capabilities, dir, damage)
    if arena_lib.is_player_in_arena(player:get_player_name(), "electroball") and
        not (arena_lib.is_player_spectating(player:get_player_name())) and
        arena_lib.is_player_in_arena(hitter:get_player_name(), "electroball") and
        not (arena_lib.is_player_spectating(hitter:get_player_name())) then
        local control = hitter:get_player_control()
        local p_name = hitter:get_player_name()
        if arena and arena.in_game and core.get_gametime() + punch_timeout >
            arena.players[p_name].last_punch then
            if control.sneak then
                player:add_player_velocity(vector.multiply(dir, -5))
            else
                player:add_player_velocity(vector.multiply(dir, 5))
            end
            arena.players[p_name].last_punch = core.get_gametime()

        end
    end
end)

-- -- leaving the arena
-- local sec = 0
-- core.register_globalstep(function(dtime)
--     sec = sec + dtime
--     if sec <= 1 then
--         sec = 0
--         for _,player in pairs(core.get_connected_players()) do
--             local p_name = player:get_player_name()
--             if arena_lib.is_player_in_arena(p_name, "electroball") and not (arena_lib.is_player_spectating(p_name)) then
--                 if core.get_node(player:get_pos()).name == "electroball:exit" then
--                     local arena = arena_lib.get_arena_by_player(p_name)
--                     player:set_properties(arena.players[p_name].old_props)
--                     player:hud_set_flags(arena.players[p_name].old_flags)
--                     arena_lib.remove_player_from_arena(p_name, 3)
--                     if #arena.players == 0 then
--                         electroball.refresh(arena)
--                     end
--                 end
--             end
--         end                
--     end
-- end)


