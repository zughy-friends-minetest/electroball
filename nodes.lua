
local modname = "electroball"


core.register_node(modname .. ":floor",{
	description = "Electroball Floor",
	tiles = {"electroball_grid.png","electroball_black.png","electroball_black.png"},
	drawtype = "glasslike",
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	use_texture_alpha = "clip", 
	pointable    = false, 
	diggable     = false, 
	buildable_to = false,  
	drop = "",
	groups = { disable_jump = 1,},
})

core.register_node(modname .. ":subfloor",{
	description = "Electroball SubFloor",
	tiles = {"electroball_black.png"},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {type="fixed", fixed={-0.5,-0.5,-0.5,0.5,1.49,0.5}},
	sunlight_propagates = true,
	is_ground_content = false,
	use_texture_alpha = "clip", 
	pointable    = false, 
	diggable     = false, 
	buildable_to = false,  
	drop = "",
	groups = {disable_jump = 1,},
})



core.register_node(modname .. ":wall", {
        description = "Electroball Wall",
        tiles = {"electroball_black.png"},
        groups = {disable_jump = 1,},
        paramtype = "light",
        sunlight_propagates = true,
})

core.register_node(modname .. ":blue_goal", {
	description = "Blue Goal",
	drawtype = "glasslike_framed",
	tiles = {"electroball_bluegoal.png", "default_obsidian_glass_detail.png"},
	use_texture_alpha = "clip", 
	paramtype = "light",
	is_ground_content = false,
	sunlight_propagates = true,
	walkable = false,
	pointable = false,	
})

core.register_node(modname .. ":red_goal", {
	description = "Red Goal",
	drawtype = "glasslike_framed",
	tiles = {"electroball_redgoal.png", "default_obsidian_glass_detail.png"},
	use_texture_alpha = "clip", 
	paramtype = "light",
	is_ground_content = false,
	sunlight_propagates = true,
	walkable = false,
	pointable = false,
})

core.register_node(modname .. ":black_glass", {
	description = "Black Glass",
	drawtype = "glasslike_framed",
	tiles = {"electroball_black.png", "default_obsidian_glass_detail.png"},
	use_texture_alpha = "clip", 
	paramtype = "light",
	is_ground_content = false,
	sunlight_propagates = true,
	walkable = true,	
})


core.register_node(modname .. ":reset", {
	description = "Electroball Reset",
	tiles = {"electroball_refresh.png"},
	paramtype = "light",
	pointable = true,
	sunlight_propagates = true,
	on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
		local p_name = clicker:get_player_name()
		local mod = arena_lib.get_mod_by_player(p_name)
		if mod == "electroball" then
			local arena = arena_lib.get_arena_by_player(p_name)

			electroball.refresh(arena)

		end 
	end,
})


core.register_node(modname .. ":exit",{
	description = "Electroball Exit",
	tiles = {"arenalib_editor_quit.png"},
	sunlight_propagates = true,
	is_ground_content = false,
	use_texture_alpha = "opaque", 
	drop = "",
	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
		local p_name = player:get_player_name()
		local mod = arena_lib.get_mod_by_player(p_name)
		if mod == "electroball" then
			local arena = arena_lib.get_arena_by_player(p_name)
			player:set_properties(arena.players[p_name].old_props)
			player:hud_set_flags(arena.players[p_name].old_flags)
			arena_lib.remove_player_from_arena(p_name, 3)
		end 
	end,
})
