local punch_timeout = electroball.punch_timeout

core.register_entity("electroball:ball", {
    _arena_name = "",
    _arena_id = nil,
    _old_velocity = vector.new(0,0,0),
    _uuid = 0,
    initial_properties = {
        hp_max = 10,
        visual = "mesh",
        glow = 20,
        static_save = true,
        mesh = "electroball.obj",
        physical = true,
        collide_with_objects = true,
        collisionbox = {-0.0625*1.5, -0.001, -0.0625*1.5, 0.0625*1.5, .125*1.5, 0.0625*1.5},
        visual_size = vector.new(15,15,15),
        textures = {"electroball_ball.png"},
    },

    get_staticdata = function(self)
        local json = core.write_json({
            _arena_id = self._arena_id,
            _arena_name = self._arena_name,
            _uuid = self._uuid,
        })
        return json
    end,


    on_step = function(self, dtime, moveresult)

        if self._arena_id then
            if arena_lib.mods["electroball"].arenas[self._arena_id].in_game == false then
                self.object:remove()
                return
            end
            if arena_lib.mods["electroball"].arenas[self._arena_id].ball_uuid ~= self._uuid then
                self.object:remove()
                return
            end
        end

        -- bouncy!
        if vector.length(self._old_velocity) > 0 then
            local vel = self.object:get_velocity()
            if vel.x == 0 then
                vel.x = - self._old_velocity.x
            end
            if vel.z == 0 then
                vel.z = - self._old_velocity.z
            end
            self.object:set_velocity(vel)
        end


        -- max speed 8.5
        local vel = self.object:get_velocity()
        local len = vector.length(vel)
        if len > 5.5 then
            self.object:set_velocity(vector.multiply(vector.normalize(vel),5.5))
        end
        self.object:set_acceleration(vector.new(0,-10,0))


        if core.get_node(self.object:get_pos()).name == "electroball:red_goal" then
            electroball.add_points(self._arena_id,2,1)
            self.object:remove()
            return
        end
        if core.get_node(self.object:get_pos()).name == "electroball:blue_goal" then
            electroball.add_points(self._arena_id,1,1)
            self.object:remove()
            return
        end
        self._old_velocity = self.object:get_velocity()
    end,
    
    on_punch = function(self, puncher, _, _, dir)
        if arena_lib.is_player_in_arena(puncher:get_player_name(), "electroball") and not (arena_lib.is_player_spectating(puncher:get_player_name())) then
            local p_name = puncher:get_player_name()
            local arena = arena_lib.get_arena_by_player(p_name)
            if core.get_gametime() + punch_timeout > arena.players[p_name].last_punch then
                dir.y=0
                if puncher:get_player_control() and puncher:get_player_control().sneak == true then
                    self.object:add_velocity(vector.multiply(dir,2))
                else
                    self.object:add_velocity(vector.multiply(dir,2))
                end
            end
        end
        self.object:set_hp(10)
        return true
    end,
    on_activate = function(self, staticdata, dtime_s)
        self.object:set_velocity(vector.new(0,-5,0))
        if staticdata ~= "" and staticdata ~= nil then
            local data = core.parse_json(staticdata) or {}
            if data._arena_id then
                self._arena_id = data._arena_id
            end
            if data._arena_name then
                self._arena_name = data._arena_name
            end
            if data._uuid then
                self._uuid = data._uuid
            end
        end
    end,
})



core.register_entity("electroball:teampoints", {
    _arena_name = "",
    _arena_id = nil,
    _team_id = nil,
    _score = 0,
    _digits = 1,
    _uuid = 0,
    -- _velocity = vector.new(0,0,0),
    initial_properties = {
        hp_max = 10,
        visual = "item",
        -- visual_size = {x=0.1,y=0.117,z=0.1},
        glow = 10,
        static_save = true,
        mesh = "electroball.obj",
        physical = true,
        visual_size = vector.new(.8,.8,.8),
        collide_with_objects = false,
        collisionbox = {-0.0625, -0.0001, -0.0625, 0.0625, 0.125, 0.0625},
        textures = {"electroball:1_0"},
    },
    get_staticdata = function(self)
        local json = core.write_json({
            _arena_id = self._arena_id,
            _arena_name = self._arena_name,
            _score = self._score,
            _team_id = self._team_id,
            _digits = self._digits,
            _uuid = self._uuid,
        })
        return json
    end,

    on_step = function(self, dtime, moveresult)

        if self._arena_id then
            if arena_lib.mods["electroball"].arenas[self._arena_id].in_game == false then
                self.object:remove()
                return
            end
            if arena_lib.mods["electroball"].arenas[self._arena_id].counter_uuid ~= self._uuid then
                self.object:remove()
                return
            end
        end
    

        local score = arena_lib.mods["electroball"].arenas[self._arena_id].teams[self._team_id].points
        local disp = 0
        if self._digits == 1 then
            disp = score % 10
        end
        if self._digits == 2 then
            disp = (score - (score % 10))/10 % 10
        end

        if self._score ~= disp then
            self._score = disp
            self.object:set_properties({textures = {"electroball:"..self._team_id.."_"..disp}})
        end

    end,
    
    on_punch = function(self, puncher, _, _, dir)
        return true
    end,
    on_activate = function(self, staticdata, dtime_s)
        if staticdata ~= "" and staticdata ~= nil then
            local data = core.parse_json(staticdata) or {}
            if data._arena_id then
                self._arena_id = data._arena_id
            end
            if data._arena_name then
                self._arena_name = data._arena_name
            end
            if data._team_id then
                self._team_id = data._team_id
            end
            if data._score then
                self._score = data._score
            end
            if data._digits then
                self._digits = data._digits
            end
            if data._uuid then
                self._uuid = data._uuid
            end
        end
        self.object:set_properties({textures = {"electroball:"..self._team_id.."_"..self._score}})

    end,
})

-- visuals for the counter

for i = 0,9 do
    core.register_craftitem("electroball:1_"..i,{
        description = "",
        inventory_image = "electroball_red_"..i..".png",
        groups = {not_in_creative_inventory = 1}
    })
    core.register_craftitem("electroball:2_"..i,{
        description = "",
        inventory_image = "electroball_blue_"..i..".png",
        groups = {not_in_creative_inventory = 1}
    })
end

-- hand item to change reach

core.register_node("electroball:hand", {
    description = "",
    tiles = {"blank.png"},
    range = 1.3,
    groups = {not_in_creative_inventory=1}
})


